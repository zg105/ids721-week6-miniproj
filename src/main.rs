use mysql_async::{prelude::*, Pool, Row};
use lambda_runtime::{LambdaEvent, Error as LambdaError, service_fn};
use serde_json::{json, Value};

#[tokio::main]
async fn main() -> Result<(), LambdaError> {
    // Create a service function using the handler function and run the Lambda function
    let func = service_fn(handler);
    lambda_runtime::run(func).await?;
    Ok(())
}

async fn handler(event: LambdaEvent<Value>) -> Result<Value, LambdaError> {

    // Connect to AWS MySQL
    let pool = Pool::new("mysql://admin:gzcgzc0721@week5.cvys4s6o2i31.us-east-1.rds.amazonaws.com:3306/week5");
    let mut conn = pool.get_conn().await?;

    // Perform the query
    let rows: Vec<Row> = conn.query("SELECT sum(quantity) AS sum FROM week5").await?;

    // Extract the 'max' column as i64 from the first row
    let result: Option<i64> = rows.get(0).and_then(|row| row.get("sum"));

    let response = match result {
        Some(sum) => json!({ "sum of quality": sum }),
        None => json!({ "sum of quality": null }),
    };
    // Serialize the response into JSON format and return it
    Ok(response)
}

