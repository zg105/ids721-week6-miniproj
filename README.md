# IDS721 Week 6 Mini Project

## Introduction

This Rust Lambda function project implemented logging through AWS Cloudwatch and  AWS X-Ray tracing. We can monitor, troubleshoot, and analyze the Lambda function's performance in the AWS ecosystem.

## Get started
I used the lambda function of Week 5 Mini Project, which calculates the sum of item quantity. 
![](pic/1.png)
In the condiguration, enabled the X-Ray active tracing and Lambda insight monitoring.
![](pic/2.png)

## Test and Result
We can run some tests using aws lambda.
After that, we can see some logs of our activity:
![](pic/3.png)
We can delve into the detail of logging
![](pic/4.png)
We can also check the tracing of lambda function:
![](pic/5.png)

